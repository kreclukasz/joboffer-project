package com.lukaszkrec.jobofferproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobofferProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobofferProjectApplication.class, args);
    }

}
